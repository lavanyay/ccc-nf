/*
 * TrafficGenerator.cc
 *
 *  Created on: Jul 20, 2015
 *      Author: lav
 */

#include <traffic/TrafficGenerator.h>


TrafficGenerator::TrafficGenerator() {
    // TODO Auto-generated constructor stub
    numTors = 1;
    hostsPerTor = 4;
    packetSize = 1.5 * 1e3 * 8;
    backgroundFsd = ConstantFlowSizeDistribution(64);
    capacity = 1e6; // TODO: make param
}

TrafficGenerator::makeBackgroundTraffic(double load, int totalNumFlows) {
    // all to all
    // poisson arrival
    // FSD !?
    double flowsPerSecond = (load * capacity)/(packetSize * backgroundFsd.getMean());
    double interFlowTime = 1.0/flowsPerSecond;

    int totalNumHosts = hostsPerTor * numTors - 1;
    int numPairs = (totalNumHosts * totalNumHosts-1)/2;

    double perPairInterFlowTime = interFlowTime/numPairs;

    int numFlows = 0;
    std::pair<int, int> pairs;
    std::map<std::pair<int, int>, SimTime> lastFlowTime;

    for (int i = 1; i < totalNumHosts; i++) {
        for (int j = 1; j < i; j++) {
            std::pair<int, int> hostPair = std::pair<int, int>(i, j);
            pairs.push_back(hostPair);
            assert(lastFlowTime.count(hostPair) == 0);
            lastFlowTime[hostPair] = 0;
        }
    }

    while(numFlows < totalNumFlows) {
        for (const auto& p: pairs) {
  //          double interval = poisson()
        }
    }


}

TrafficGenerator::~TrafficGenerator() {
    // TODO Auto-generated destructor stub
}

