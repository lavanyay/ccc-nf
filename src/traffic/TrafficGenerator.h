/*
 * TrafficGenerator.h
 *
 *  Created on: Jul 20, 2015
 *      Author: lav
 */

#ifndef TRAFFICGENERATOR_H_
#define TRAFFICGENERATOR_H_

class TrafficGenerator {
    int numTors;
    int hostsPerTor;
    int packetSize; // bits
    double capacity;

    FlowSizeDistribution backgroundFsd;
public:
    TrafficGenerator();
    virtual ~TrafficGenerator();
};

#endif /* TRAFFICGENERATOR_H_ */
