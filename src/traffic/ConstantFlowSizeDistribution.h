/*
 * ConstantFlowSizeDistribution.h
 *
 *  Created on: Jul 20, 2015
 *      Author: lav
 */

#ifndef CONSTANTFLOWSIZEDISTRIBUTION_H_
#define CONSTANTFLOWSIZEDISTRIBUTION_H_

class ConstantFlowSizeDistribution : public FlowSizeDistribution {
private:
    int mean;

public:
    ConstantFlowSizeDistribution(int mean) : mean(mean) {};
    virtual ~ConstantFlowSizeDistribution();

    virtual int getMean();
    virtual int getSample();
};

#endif /* CONSTANTFLOWSIZEDISTRIBUTION_H_ */
