/*
 * FlowSizeDistribution.h
 *
 *  Created on: Jul 20, 2015
 *      Author: lav
 */

#ifndef FLOWSIZEDISTRIBUTION_H_
#define FLOWSIZEDISTRIBUTION_H_

class FlowSizeDistribution {
public:
    FlowSizeDistribution();
    virtual ~FlowSizeDistribution();
    virtual int getMean();
    virtual int getSample();
};

#endif /* FLOWSIZEDISTRIBUTION_H_ */
