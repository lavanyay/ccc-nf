/*
 * Scheduler.h
 *
 *  Created on: Jul 6, 2015
 *      Author: lav
 */

#ifndef SCHEDULER_H_
#define SCHEDULER_H_


#include "App.h"
#include "Packet_m.h"
#include "FlowSchedule.h"
#include "Waterfilling.h"

#include <memory>
#include <omnetpp.h>

class Scheduler : public App
{
  private:
    // configuration

    // state
    std::map<int, FlowSchedule> completedFlows;
    std::map<int, FlowSchedule> flowTable;
    std::map<Link, double> linkCapacities;
    std::map<Link, SimTime> linkDelays;
    std::map<std::pair<int, int>, Path> shortestPaths;
    std::map<std::pair<int, int>, SimTime> minRtts;

    std::vector<std::pair<SimTime, ControlPacket*> > requests;
    std::vector<std::pair<SimTime, ControlPacket*> > endNotifications;

    SimTime timeoutInterval;
    TimeoutMessage timeout;
  public:
    Scheduler();
    virtual ~Scheduler();

  protected:
    virtual void initialize();
    virtual void finish();
    virtual void handleMessage(cMessage *msg);

    void loadTopology();

    //void updatePacketsLeft();
    void processEndNotification(const SimTime& time, const ControlPacket& pkt);
    void processRequest(const SimTime& time, const ControlPacket& pkt);

    // FlowTable has all flows that haven't sent an endNotification
    void doWaterfilling();
    void updateRates();
    //void updateEndTimes();

    void printNetworkState();
    void printFlowState();

    std::unique_ptr<Waterfilling> wf_;
    ControlPacket* makeGrantPacket(const ControlPacket& requestPkt, double rate);
};


#endif /* SCHEDULER_H_ */
