//
// This file is part of an OMNeT++/OMNEST simulation example.
//
// Copyright (C) 1992-2008 Andras Varga
//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//

#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include <vector>
#include <omnetpp.h>
#include "Scheduler.h"
#include <assert.h>
void Scheduler::doWaterfilling()
{

    std::map<Flow, std::set<Link>> flowToLinks;
    std::map<Link, double> remainingCapacity;

    for (const auto& fs: flowTable) {
        std::set<Link> setOfLinks;
        const Path& path = shortestPaths.at(std::pair<int, int>(fs.second.source, fs.second.destination));

        for (const auto& l: path) {
            if (remainingCapacity.count(l) == 0) {
                remainingCapacity[l] = linkCapacities.at(l);
            }
            setOfLinks.insert(l);
        }
        flowToLinks[fs.first] = setOfLinks;
    }

    wf_->initialize(flowToLinks, remainingCapacity);

    assert(wf_->getWaterfillingState() == INITIALIZED);

    printFlowState();
    ev << "before waterfilling.\n";
    ev << "initialized waterfilling with\n";

    for (const auto& fl: flowToLinks) {
        ev << "Flow " << fl.first << " uses links ";
        for (const auto& l: fl.second) {
            ev << " (" << l.first << ", " << l.second << ") [" << remainingCapacity.at(l)/1e+9 << " Gbps]";
        }
        ev << ".\n";
    }


    wf_->doWaterfilling();

    //generatePacket = NULL;

    assert(wf_->getWaterfillingState() == WATERFILLED);
}



