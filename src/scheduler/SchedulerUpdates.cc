#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include "Scheduler.h"

#include <omnetpp.h>
#include <assert.h>

#include <vector>
#include <cmath>

// based on rates, firstPacket..

// based on new rates from water filling, rates, firstPacketTime
void Scheduler::updateRates()
{

    SimTime now = simTime();

    ev << "Update " << flowTable.size() << " rates after waterfilling at " << now << ".\n";
    //generatePacket = NULL;

    assert(wf_->getWaterfillingState() == WATERFILLED);
    std::map<Flow, double> flowRates = wf_->getOptimalRates();

    for (auto& fs : flowTable) {
        double newRate = flowRates.at(fs.first);
        assert(newRate > 0); // why?

        ev << "Changed rate of " << fs.first << " from original " << fs.second.rate << " to " << newRate << ".\n";

        assert(fs.second.packetsLeft > 0);
        fs.second.rate = newRate;
        fs.second.rateSchedule[now] = newRate;

    }
    printFlowState();
}


