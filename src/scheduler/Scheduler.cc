//
// This file is part of an OMNeT++/OMNEST simulation example.
//
// Copyright (C) 1992-2008 Andras Varga
//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//

#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include <vector>
#include <omnetpp.h>
#include <assert.h>
#include "Packet_m.h"
#include "FlowSchedule.h"
#include "App.h"
#include "Scheduler.h"

/**
 * Handler flow rate requests.
 */


Define_Module(Scheduler);


Scheduler::Scheduler()
{
    //generatePacket = NULL;
}

void Scheduler::finish()
{
    cancelEvent(&timeout);

}
Scheduler::~Scheduler()
{

}

void Scheduler::initialize()
{
    App::initialize();

    timeoutInterval = par("timeoutInterval");
    wf_.reset(new Waterfilling);

    loadTopology();
    printNetworkState();

    statNames = {"flowId", "flowSize", "flowMinFct", "flowCompletionTime", "flowStretch"};
    setupSignals();

    SimTime now = simTime();
    scheduleAt(now, &timeout);
}

void Scheduler::loadTopology()
{

    cTopology topo;
    ev << "Looking for nodes of with parameter inTopo.\n";
    topo.extractByParameter("inTopo");

    ev << "Got " << topo.getNumNodes() << " with parameter inTopo..\n";
    std::map<int, int> moduleIdToI;
    for (int i=0; i<topo.getNumNodes(); i++)
    {
      cTopology::Node *node = topo.getNode(i);
      // Hoping it just matches up with address, scheduler 0.

      ev << "Node i=" << i << " is " << node->getModule()->getFullPath()\
               << endl;

      int moduleId = node->getModuleId();
      if (moduleIdToI.count(moduleId) == 0)
          moduleIdToI[moduleId] = i;


      ev << " It has " << node->getNumOutLinks() << " conns to other nodes\n";
      ev << " and " << node->getNumInLinks() << " conns from other nodes\n";

      ev << " Connections to other modules are:\n";
    }

    for (int i=0; i<topo.getNumNodes(); i++)
    {
          cTopology::Node *node = topo.getNode(i);
          // Hoping it just matches up with address, scheduler 0.
          ev << "Node i=" << i << " is " << node->getModule()->getFullPath() << endl;
          for (int j=0; j<node->getNumOutLinks(); j++)
          {
              cGate *gate = node->getLinkOut(j)->getLocalGate();
              cTopology::Node *neighbour = node->getLinkOut(j)->getRemoteNode();

              int otherModuleId = neighbour->getModuleId();
              int otherI = moduleIdToI.at(otherModuleId);

              Link newLink = Link(i, otherI);

              cDatarateChannel *chan = check_and_cast<cDatarateChannel *>(gate->getChannel());
              double capacity = chan->getDatarate();


              linkCapacities[newLink] = capacity;
              SimTime delay = chan->getDelay();
              linkDelays[newLink] = delay;
              ev << " " << neighbour->getModule()->getFullPath()
                   << " through gate " << gate->getFullName() << endl;
        }
    }

    for (int i=0; i < topo.getNumNodes(); i++) {
        topo.calculateUnweightedSingleShortestPathsTo(topo.getNode(i));
        for (int j=0; j < topo.getNumNodes(); j++) {
            Path path;
            if (topo.getNode(j)->getNumPaths() == 0) {
                ev << "No paths from " << j << " to " << i << ".\n";
                continue;
            }
            SimTime oneWayDelay = 0;
            cTopology::Node *next = topo.getNode(j);
            assert(moduleIdToI[next->getModuleId()] == j);
            int length = 0;
            while(next != topo.getNode(i) and length < 10) {
                cTopology::Node *otherNode = next->getPath(0)->getRemoteNode();
                Link link = Link(moduleIdToI[next->getModuleId()], moduleIdToI[otherNode->getModuleId()]);
                oneWayDelay += linkDelays.at(link);
                path.push_back(link);
                next = otherNode;
                length++;
            }
            shortestPaths[std::pair<int, int>(j, i)] = path;
            minRtts[std::pair<int, int>(j, i)] = oneWayDelay * 2;
        }
    }

}

void Scheduler::printNetworkState() {
    for (const auto& lc: linkCapacities) {
        ev << "Link (" << lc.first.first << ", " << lc.first.second << ") : " << lc.second/1e+9 << " Gbps.\n";
    }
    for (const auto& sp : shortestPaths) {
        ev << "Path from " << sp.first.first << ", " << sp.first.second << ": ";
        for (const auto& l : sp.second)
            ev << "(" << l.first << ", " << l.second << "), ";
        ev << ".\n";
    }
}

void Scheduler::printFlowState() {
    for ( const auto& fs: flowTable) {
        ev << "Flow " << fs.first << ": " << fs.second.rate << " bps, " << fs.second.packetsLeft << " packets left.\n";
    }
}


ControlPacket* Scheduler::makeGrantPacket(const ControlPacket& pkt, double rate) {

    std::stringstream ss;
    ss << "grant rate for flow " << pkt.getFlowId()\
       << " \n(size " << pkt.getSize() << ", pkt size " << pkt.getPacketSize()\
                << ",\n rate " << rate/1.0e9 << " Gbps"\
                << ",\n flow src " << pkt.getFlowSource() << ", flow dest " << pkt.getFlowDestination()\
                << ",\n pkt source " << myAddress << ", pkt dest " << pkt.getFlowSource() << ")";

    ControlPacket* grantPacket = new ControlPacket(ss.str().c_str());
    grantPacket->setByteLength(0); // TODO: for instantaneous updates, change later
    grantPacket->setSource(myAddress);

    //assert(pkt.getFlowSource() == pkt.getSource());
    grantPacket->setDestination(pkt.getFlowSource());

    grantPacket->setFlowSource(pkt.getFlowSource());
    grantPacket->setFlowDestination(pkt.getFlowDestination());

    grantPacket->setFlowId(pkt.getFlowId());
    grantPacket->setRate(rate); // number of packets
    grantPacket->setControlPacketType(GRANT);
    return grantPacket;
}

void Scheduler::processRequest(const SimTime& time, const ControlPacket& pkt) {
    int flowId = pkt.getFlowId();
    assert(flowTable.count(flowId) == 0);
    flowTable[flowId] = FlowSchedule(pkt);
    FlowSchedule& fs = flowTable.at(flowId);
    fs.firstRequestTime = time;
    EV << "Flow " << flowId << " from " << fs.source << " to " << fs.destination\
            << " though path ";
    for (const auto& l : fs.path)
        ev << "(" << l.first << ", " << l.second << "), ";
    ev << ".\n";
    fs.path = shortestPaths.at(std::pair<int, int>(fs.source, fs.destination));
}

void Scheduler::processEndNotification(const SimTime& time, const ControlPacket& pkt) {
    int flowId = pkt.getFlowId();

    if (flowTable.count(flowId) == 0) {
        ev << "never seen " << flowId << ", don't know what to do with end notification.\n";
        return;
    }
    assert(flowTable.count(flowId) > 0);
    FlowSchedule& fs = flowTable.at(flowId);
    fs.endNotificationTime = time;
    assert(!fs.requestPacket->isScheduled());
    cancelAndDelete(flowTable.at(flowId).requestPacket);

    double fct = -1;
    if (fs.firstRequestTime >= SimTime(0) and fs.endNotificationTime >= SimTime(0)) fct = (fs.endNotificationTime-fs.firstRequestTime).dbl();
    emitSignal("flowId", fs.flowId);
    double size = fs.packetsLeft*fs.packetSize;
    emitSignal("flowSize", size);
    std::pair<int, int> ends = std::pair<int, int>(fs.source,fs.destination);
    Link firstLink = shortestPaths.at(ends).at(0);
    double cap = linkCapacities.at(firstLink);
    double rtt = minRtts.at(ends).dbl();
    double minFct = size/cap + rtt;
    emitSignal("flowMinFct", minFct);
    emitSignal("flowCompletionTime", fct);
    emitSignal("flowStretch", fct/minFct);

    completedFlows[flowId] = fs;
    flowTable.erase(flowId);
}



void Scheduler::handleMessage(cMessage *msg)
{
    MessageKind timeout = TIMEOUT;
    MessageKind control = CONTROL;
    MessageKind data = DATA;
    MessageKind meta = META;

    bool updateSchedule = false;

    SimTime now = simTime();
    if (msg->getKind() == control)
    {
        ControlPacket *pkt = check_and_cast<ControlPacket *>(msg);
        ControlPacketType request = REQUEST;
        ControlPacketType grant = GRANT;
        if (pkt->getControlPacketType() == request and pkt->getSize() > 0)
        requests.push_back(std::pair<SimTime, ControlPacket* >(now, pkt));
        else if (pkt->getControlPacketType() == request and pkt->getSize()== 0) {
            endNotifications.push_back(std::pair<SimTime, ControlPacket* >(now, pkt));
        }



    }
     else  if (msg->getKind() == timeout) {
         if (requests.size() > 0 or endNotifications.size() > 0) {

         ev << "Timeout at " << now << ", processing " << requests.size()\
                 << " requests and " << endNotifications.size() << " end notifications.\n";
         for (auto& request: requests) {
             ControlPacket* r = request.second;
             processRequest(request.first, *r);
             delete request.second;
             request.second = 0;
         }
         for (auto& endNotification: endNotifications) {
             ControlPacket* en = endNotification.second;
             ev << "Processing end of " << en->getFlowId();

             processEndNotification(endNotification.first, *en);
             delete en;
             endNotification.second = 0;

         }

         if (requests.size() > 0 or endNotifications.size() > 0) {
             doWaterfilling();
             updateRates();

             for (auto& fs: flowTable) {
                 ControlPacket* grantPacket =\
                         makeGrantPacket(*fs.second.requestPacket, fs.second.rate);
                 EV << "Scheduler: Sending grant packet for flow "\
                         << fs.first << ".\n";
                 send(grantPacket, "out");
             }
         }
         requests.clear();
         endNotifications.clear();
         }
         scheduleAt(now+timeoutInterval, &(this->timeout));

     } else {
        EV << "Ignoring packet that's not a rate request/ timeout.\n";
    }


    // LOGGING

    // Handle incoming packet
    if (msg->getKind() != TIMEOUT) {
        Packet *pk = check_and_cast<Packet *>(msg);
        EV << "Scheduler: received packet " << pk->getName() << " after "\
                << pk->getHopCount() << " hops" << endl;
        emit(endToEndDelaySignal, simTime() - pk->getCreationTime());
        emit(hopCountSignal, pk->getHopCount());
        emit(sourceAddressSignal, pk->getSource());
    }

    if (ev.isGUI())
    {
        getParentModule()->getDisplayString().setTagArg("i",1,"green");
        getParentModule()->bubble("Arrived!");
    }
}


