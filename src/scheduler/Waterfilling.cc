/*
 * Waterfilling.cc
 *
 *  Created on: Jul 6, 2015
 *      Author: lav
 */

#include "Waterfilling.h"
#include <omnetpp.h>

#include <algorithm>
#include <assert.h>

Waterfilling::Waterfilling(): waterfillingState(UNINITIALIZED) {};
Waterfilling::~Waterfilling() {}

void Waterfilling::initialize(const std::map<Flow, std::set<Link>>& flowToLinks,\
			      const std::map<Link, double>& remainingCapacity) {
  this->waterfillingState = UNINITIALIZED;
  this->flowToLinks = flowToLinks;
  this->remainingCapacity = remainingCapacity;
  this->unsaturatedFlows.clear();
  this->unsaturatedLinks.clear();
  this->flowRates.clear();
  this->linkToFlows.clear();


  for (const auto& lc: remainingCapacity)
    if (!(remainingCapacity.at(lc.first) > 0)) return;

  // all links in flowToLinks are in remaining capacity
  for (const auto& fl: flowToLinks) {
      this->flowRates[fl.first] = 0;
      for (const auto& l: fl.second) {
	if (!(remainingCapacity.count(l) > 0)) return;
      }
      // all links have remaining capacity > 0
      this->unsaturatedFlows.insert(fl.first);     
  }

  for (const auto& fl: flowToLinks) {
    // they're unsaturated, with one unsaturated flow
    // since remaining capacity > 0, all flows are unsaturated
    for (const auto& l: fl.second) {

      if (linkToFlows.count(l) == 0) {
	this->unsaturatedLinks.insert(l);
	linkToFlows[l] = std::set<Flow>();
      }
      linkToFlows.at(l).insert(fl.first);
    }
  }

  // all links in remaining capacity are in linkToFlows -> were in flowToLinks
  for (const auto& lc: remainingCapacity) {
    if (!(this->linkToFlows.count(lc.first) > 0)) return;
  }
  this->waterfillingState = INITIALIZED;

}

std::map<Flow, double> Waterfilling::getOptimalRates() const {
  return flowRates;
}

// re-orders linkProps, fills in minLinks, minimum
void Waterfilling::getMinimumLinks(std::vector<LinkProp>& linkProps, std::set<Link>& minLinks, double* minimum) {
    // there might be more than one links
     if (linkProps.empty()) return;
     std::make_heap(linkProps.begin(), linkProps.end());

     LinkProp minLP = linkProps.front();
     std::pop_heap(linkProps.begin(), linkProps.end()); linkProps.pop_back();

     double minInc = minLP.second;
     *minimum = minInc;

     while(minLP.second == minInc) { // double comparison
         minLinks.insert(minLP.first);
         if (linkProps.empty()) break;

         minLP = linkProps.front();
	 std::pop_heap(linkProps.begin(), linkProps.end()); linkProps.pop_back();
     }
}

// later incremental?
void Waterfilling::doWaterfilling() {
  if(this->waterfillingState != INITIALIZED) return;

    // dealing with floating point rates etc.
    // do exact increments, remaining capacity update
    // later round down rates --> remaining capacities
    // if any spare capacity left divide it unevenly among unsaturated flows

    // increment flows by (int) inc or (round,2) inc etc. (conservative)
    // link's remaining capacity -= ^ number of flows (exact)
    // ??      
    while(unsaturatedFlows.size() >  0) {

        // get links with min. increment
        std::vector<LinkProp> linkIncrements;
        for (const auto& lf: linkToFlows) {
	  double inc =((double) remainingCapacity.at(lf.first))/lf.second.size();
            LinkProp lp = LinkProp(lf.first, inc);
            ev << "Increment at link (" << lf.first.first << ", " << lf.first.second << "): " << inc/1.0e+9 << "Gbps.\n";
            linkIncrements.push_back(lp);
        }
        std::set<Link> newSaturatedLinks;
        double minInc = -1;
        getMinimumLinks(linkIncrements, newSaturatedLinks, &minInc);
        assert(minInc != -1);
        ev << "minimum increment is " << minInc << ", " << newSaturatedLinks.size() << " newly saturated links.\n";
        // mark all these minLinks as saturated, and their flows too
        // maybe deal with int later..

        // increase flow rate of every flow by minInc
        std::set<Flow> newSaturatedFlows;
        for (const auto& fl: flowToLinks) {
            ev << "Incrementing rate of flow " << fl.first << " from " << flowRates.at(fl.first) << " to ";
            flowRates.at(fl.first) += minInc;
            ev << flowRates.at(fl.first) << ".\n";

            bool saturated = false;
            for (const auto& l : newSaturatedLinks) {
                if (fl.second.count(l) > 0) {
                    saturated = true;
                    break;
                }
            }
            if (saturated) newSaturatedFlows.insert(fl.first);
        }


        // update remaining capacity of links
        for (const auto& lf: linkToFlows) {
            // assert sum of flows's rate = total capacity
            // assert inc * num. of unsaturated flows = remaining capacity
	  if (newSaturatedLinks.count(lf.first) > 0) remainingCapacity[lf.first] = 0;
            else {
                double totalInc = minInc * lf.second.size();
                remainingCapacity[lf.first] -= totalInc;
            }
         }

        // update mappings- remove saturated flows and links
        for (const auto& f: newSaturatedFlows) 
	  {unsaturatedFlows.erase(f); flowToLinks.erase(f);}
        for (const auto& l: newSaturatedLinks) 
	  {unsaturatedLinks.erase(l); linkToFlows.erase(l);}
     }

    this->waterfillingState = WATERFILLED;
}

