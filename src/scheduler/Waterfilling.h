/*
 * Waterfilling.h
 *
 *  Created on: Jul 7, 2015
 *      Author: lav
 */

#ifndef WATERFILLING_H_
#define WATERFILLING_H_

#include <map>
#include <set>
#include <vector>

typedef int Flow;
typedef std::pair<int, int> Link;
typedef std::pair<Link, double> LinkProp;
typedef enum{UNINITIALIZED, INITIALIZED, WATERFILLED} WaterfillingState;


class Waterfilling
{
  private:
    // configuration

    // state
    std::map<Flow, std::set<Link>> flowToLinks; // only unsaturated links per flow
    std::map<Link, std::set<Flow>> linkToFlows; // only unsaturated flows on line
    std::map<Link, double> remainingCapacity; 
    std::map<Flow, double> flowRates; 
    std::set<Link> unsaturatedLinks;
    std::set<Flow> unsaturatedFlows;
    WaterfillingState waterfillingState;
  public:
    Waterfilling();
    virtual ~Waterfilling();

    void initialize(const std::map<Flow, std::set<Link>>& flowToLinks,\
		    const std::map<Link, double>& remainingCapacity);
    // check pre-reqs: every link in linksPerFlow has remainginCapacity > 0
    // every link in remainingCapacity is used by some flow
    void doWaterfilling();

    std::map<Flow, double> getOptimalRates() const;
    WaterfillingState getWaterfillingState() {return waterfillingState;}

    // note return integer rates, not exactly optimal (so links may have spare cap), but definitely feasible
  protected:
    void getMinimumLinks(std::vector<LinkProp>& linkProps,\
			 std::set<Link>& minLinks, double* minimum);
};




#endif /* WATERFILLING_H_ */
