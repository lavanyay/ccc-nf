/*
 * WaterfillingTest.cc
 *
 *  Created on: Jul 7, 2015
 *      Author: lav
 */
/*
#include "experimental/lav/waterfilling/Waterfilling.h"

#include <memory>
#include <string>
#include <map>
#include <set>

#include "testing/base/public/gmock.h"
#include "testing/base/public/googletest.h"
#include "testing/base/public/gunit.h"

using ::testing::ElementsAre;
using ::testing::WhenSorted;
using ::testing::Pair;

namespace unittesting {
  namespace {

    class WaterfillingTest : public testing::Test {
    protected:
      void SetUp() override {
	// Code here will be called before *each* test.
	wf_.reset(new Waterfilling);
	}

	// Code in TearDown will be called after *each* test.
	// void TearDown() override {}

      std::unique_ptr<Waterfilling> wf_;

      void oneLinkTwoFlows(std::map<Flow, std::set<Link>>& flowToLinks,	\
				  std::map<Link, double>& remainingCapacity);

      };

      void WaterfillingTest::oneLinkTwoFlows(std::map<Flow, std::set<Link>>& flowToLinks,\
        std::map<Link, double>& remainingCapacity) {
	Link l = Link(0, 1);

	flowToLinks[0] = {l};
	flowToLinks[1] = {l};

	remainingCapacity[l] = 1;
      };

      // const char WaterfillingTest::kGoodDataFile[] =
      // 	"/google3/codelab/unittesting/cpp/synonyms-good1.txt";

      TEST_F(WaterfillingTest, Initialization) {
	   std::map<Flow, std::set<Link>> flowToLinks;
	   std::map<Link, double> remainingCapacity;
	   oneLinkTwoFlows(flowToLinks, remainingCapacity);

	   EXPECT_TRUE(wf_->getWaterfillingState() == UNINITIALIZED);

	   wf_->initialize(flowToLinks, remainingCapacity);


	   EXPECT_TRUE(wf_->getWaterfillingState() == INITIALIZED);


	   std::map<Flow, double> flowRates;

	   flowRates = wf_->getOptimalRates();
	   EXPECT_TRUE(flowRates.size() == 2);

	   
      }

      TEST_F(WaterfillingTest, LoadTwoBottleneck) {
	   std::map<Flow, std::set<Link>> flowToLinks;
	   std::map<Link, double> remainingCapacity;
	   Link link01 = Link(0,1);
	   Link link12 = Link(1,2);
	   Link link13 = Link(1,3);

	   remainingCapacity[link01] = 30.0;
	   remainingCapacity[link12] = 20.0;
	   remainingCapacity[link13] = 10.0;

	   flowToLinks[0] = {link01, link12};
	   flowToLinks[1] = {link01, link12};
	   flowToLinks[2] = {link01, link13};
	   flowToLinks[3] = {link01, link13};

	   wf_->initialize(flowToLinks, remainingCapacity);
	   EXPECT_TRUE(wf_->getWaterfillingState() == INITIALIZED);

	   wf_->doWaterfilling();
	   EXPECT_TRUE(wf_->getWaterfillingState() == WATERFILLED);

	   std::map<Flow, double> flowRates;
	   flowRates = wf_->getOptimalRates();

	   EXPECT_TRUE(flowRates.size() == 4);
	   EXPECT_THAT(flowRates,  WhenSorted(ElementsAre(Pair(0, 10.0), Pair(1, 10.0), Pair(2, 5.0), Pair(3, 5.0))));
	     
      }

      TEST_F(WaterfillingTest, LoadSimple) {
	   std::map<Flow, std::set<Link>> flowToLinks;
	   std::map<Link, double> remainingCapacity;
	   oneLinkTwoFlows(flowToLinks, remainingCapacity);

	   EXPECT_TRUE(wf_->getWaterfillingState() == UNINITIALIZED);

	   wf_->initialize(flowToLinks, remainingCapacity);


	   EXPECT_TRUE(wf_->getWaterfillingState() == INITIALIZED);

	   std::map<Flow, double> flowRates;
	   
	   wf_->doWaterfilling();

	   EXPECT_TRUE(wf_->getWaterfillingState() == WATERFILLED);

	   flowRates = wf_->getOptimalRates();
	   EXPECT_TRUE(flowRates.size() == 2);
	   EXPECT_TRUE(flowRates.count(0) > 0);
	   EXPECT_TRUE(flowRates[0] = 0.5);
	   EXPECT_TRUE(flowRates.count(1) > 0);
	   EXPECT_TRUE(flowRates[1] = 0.5);

	   
      }

    // One link, many many flows

    // Few links, many many flows, so you run into floating point bugs.

    // Do you want to make bottleneck link number available?

      // TEST_F(WaterfillingTest, GetSynonymsReturnsListOfWords) {
      // 	EXPECT_TRUE(synonyms_->LoadDictionary(FLAGS_test_srcdir + kGoodDataFile));

      // 	// Retrieve a list of synonyms for a basic word; the results should be sorted
      // 	// in alphabetical order.
      // 	//
      // 	// This uses a gMock matcher (::testing::ElementsAre), rather than a
      // 	// Waterfilling::SynonymVector and EXPECT_EQ, for a more concise syntax and
      // 	// better error messages. http://go/matchers
      // 	EXPECT_THAT(*(synonyms_->GetSynonyms("cat")),
      // 		    ElementsAre("feline", "kitty", "tabby"));
      // }

      // TEST_F(WaterfillingTest, GetSynonymsWhenNoSynonymsAreAvailable) {
      // 	// Test for a word that's in the file but with no synonyms.

      // 	// Test for a word that isn't in the file.
      // }

    }  // namespace
  }  // namespace unittesting


*/
