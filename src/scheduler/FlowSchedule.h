#ifndef FLOW_SCHEDULE_H
#define FLOW_SCHEDULE_H

#include "Packet_m.h"
#include <vector>

typedef std::vector<std::pair<int,int>> Path;

class FlowSchedule {
public:
    int flowId;
    int source;
    int destination;
    int packetSize; // bits
    int packetsLeft;

    double rate;

    std::map<SimTime, double> rateSchedule; // not used

    Path path;

    // not used
    SimTime firstRequestTime;
    SimTime endNotificationTime;

    ControlPacket* requestPacket;


    FlowSchedule(const ControlPacket& pkt) : flowId(pkt.getFlowId()), source(pkt.getFlowSource()), destination(pkt.getFlowDestination()),\
            packetsLeft(pkt.getSize()), rate(-1), packetSize(pkt.getPacketSize()),\
            firstRequestTime(SimTime(-1)), endNotificationTime(SimTime(-1)),\
            requestPacket(new ControlPacket(pkt)){}
    FlowSchedule() : flowId(-1), source(-1), destination(-1), packetsLeft(-1), rate(-1), packetSize(-1),\
            firstRequestTime(SimTime(-1)),   endNotificationTime(SimTime(-1)),\
            requestPacket(0){}
};

#endif // FLOW_SCHEDULE_H
