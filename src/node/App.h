#ifndef APP_H
#define APP_H

#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include <vector>
#include <omnetpp.h>
#include "Packet_m.h"
#include "FlowInfo.h"

/**
 * Generates traffic for the network.
 */
class App : public cSimpleModule
{
  protected:
    // configuration
    int myAddress;


    // state
    //cMessage *generatePacket;
    //long pkCounter;
    std::map<int, FlowInfo> flowTable;

    // signals
    simsignal_t endToEndDelaySignal;
    simsignal_t hopCountSignal;
    simsignal_t sourceAddressSignal;

    std::map<std::string, simsignal_t> signals;
    std::vector<std::string> statNames;
    std::vector<std::string> flowStatNames;
    std::set<int> flowsWithSignals;

  public:
    App();
    virtual ~App();

  protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);

    ControlPacket* makeRequestPacket(const MetaPacket& pkt);
    void updateRate(int flowId, double newRate);
    void nextPacket(int flowId);

    void setupSignals();
    void setupFlowSignals(int flowId);
    void emitFlowSignal(int flowId, const std::string& name, double value);
    void emitSignal(std::string name, double value);
};

#endif //APP_H
