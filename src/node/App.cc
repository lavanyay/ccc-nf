//
// This file is part of an OMNeT++/OMNEST simulation example.
//
// Copyright (C) 1992-2008 Andras Varga
//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//

#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include <vector>
#include <omnetpp.h>
#include "Packet_m.h"
#include "FlowInfo.h"
#include "App.h"
#include <assert.h>

Define_Module(App);


App::App()
{
}

App::~App()
{
    for (auto& f: flowTable) {
        cancelAndDelete(f.second.nextPacket);
    }
}

void App::initialize()
{
    myAddress = par("address");



    //WATCH(pkCounter);
    WATCH(myAddress);

    //const char *destAddressesPar = par("destAddresses");
    //cStringTokenizer tokenizer(destAddressesPar);
    //const char *token;
    //while ((token = tokenizer.nextToken())!=NULL)
    //    destAddresses.push_back(atoi(token));

    //if (destAddresses.size() == 0)
    //    throw cRuntimeError("At least one address must be specified in the destAddresses parameter!");

    //generatePacket = new cMessage("nextPacket");
    //scheduleAt(sendIATime->doubleValue(), generatePacket);


    flowStatNames = {"queueSize", "rtt"};


    endToEndDelaySignal = registerSignal("endToEndDelay");
    hopCountSignal =  registerSignal("hopCount");
    sourceAddressSignal = registerSignal("sourceAddress");
}

ControlPacket* App::makeRequestPacket(const MetaPacket& pkt) {

    std::stringstream ss;
    ss << "rate request for flow " << pkt.getFlowId()\
            << "\n (size " << pkt.getSize() << ", pkt size " << pkt.getPacketSize()\
            << ",\n flow src " << pkt.getSource() << ", flow dest " << pkt.getDestination()\
            << ",\n pkt source " << myAddress << ", pkt dest " << pkt.getSchedulerAddress() << ")";

    ControlPacket* requestPacket = new ControlPacket(ss.str().c_str());
    requestPacket->setSource(myAddress);
    requestPacket->setDestination(pkt.getSchedulerAddress());



    requestPacket->setFlowSource(pkt.getSource());
    requestPacket->setFlowDestination(pkt.getDestination());

    requestPacket->setFlowId(pkt.getFlowId());
    requestPacket->setSize(pkt.getSize()); // number of packets
    requestPacket->setPacketSize(pkt.getPacketSize());
    requestPacket->setControlPacketType(REQUEST);
    return requestPacket;
}


void App::updateRate(int flowId, double newRate) {
    FlowInfo& flowInfo = flowTable.at(flowId);
    double oldRate = flowInfo.rate;
    flowInfo.rate = newRate;
    SimTime now = SimTime();
    if (newRate != oldRate and newRate > 0) {
        if (!flowInfo.nextPacket->isScheduled()) {
            EV << "No timeout scheduled yet for flow " << flowId << ".\n";
            assert(flowInfo.packetsSent == flowInfo.size or oldRate == -1);
            if (flowInfo.packetsSent == flowInfo.size) return;
            nextPacket(flowId);
        } else {
            EV << "Cancel scheduled timeout for flow " << flowId << ".\n";
            cancelEvent(flowInfo.nextPacket);

            SimTime oldInterval = SimTime(flowInfo.packetSize/oldRate);
            SimTime oldScheduledTime = oldInterval + flowInfo.lastPacketSent;

            ev << " new rate is " << newRate << " and old rate was " << oldRate << ".\n";
            ev << " packet size is " << flowInfo.packetSize << "\n";

            SimTime interval = SimTime(flowInfo.packetSize/flowInfo.rate); // newRate
            ev << " new interval is " << interval << " and old interval was " << oldInterval << ".\n";
            SimTime newScheduledTime = flowInfo.lastPacketSent + interval;

            ev << "old scheduled time for next packet was " << oldScheduledTime\
                    << ", new time is " << newScheduledTime\
                    << ", now time is " << now << ".\n";

            if (newScheduledTime < now) nextPacket(flowId);
            else scheduleAt(newScheduledTime, flowInfo.nextPacket);
        }
        // if nothing scheduled, if packets left do nextPacket
        // otherwise nextPacket should be at lastPacket+interval
        // do it now or wait.
    }

    if (ev.isGUI()) getParentModule()->bubble("Updating rate...");

}

void App::nextPacket(int flowId) {
    // Sending packet
    //int destAddress = destAddresses[intuniform(0, destAddresses.size()-1)];

    SimTime now = simTime();
    FlowInfo& flowInfo = flowTable.at(flowId);

    char pkname[40];
    sprintf(pkname,"pk-%d-to-%d-#%dof#%d", flowInfo.source, flowInfo.destination, flowInfo.packetsSent, flowInfo.size);
    EV << "App " << myAddress << ": Generating next data packet " << pkname << endl;

    DataPacket *pk = new DataPacket(pkname);
    pk->setByteLength(flowInfo.packetSize/8);
    pk->setSource(flowInfo.source);
    pk->setDestination(flowInfo.destination);
    pk->setFlowId(flowId);
    pk->setHopCount(0);
    pk->setTs(now.dbl());

    int seqNoOfThisPacket = flowInfo.packetsSent;

    assert(seqNoOfThisPacket < flowInfo.size);

    if (seqNoOfThisPacket < flowInfo.size-1)
        pk->setDataPacketType(JUST_DATA);
    else if (seqNoOfThisPacket == flowInfo.size-1) {
        pk->setDataPacketType(FIN);
        EV << "It's a FIN!\n";
    }

    send(pk,"out");
    flowInfo.lastPacketSent = simTime();
    flowInfo.packetsSent++;
    // can't access pk after?
    if (flowInfo.rate > 0 and seqNoOfThisPacket < flowInfo.size-1) {
        SimTime interval = SimTime(((double) flowInfo.packetSize)/flowInfo.rate);
        SimTime nextTime = simTime() + interval;
        EV << "Just sent packet # " << seqNoOfThisPacket << " of flow " << flowId << " at time " << flowInfo.lastPacketSent << ", scheduling next timeout at " << nextTime << "s.\n";
        scheduleAt(nextTime, flowInfo.nextPacket);
    }

    if (ev.isGUI()) getParentModule()->bubble("Generating packet...");
}

void App::setupFlowSignals(int flowId) {
    for (const auto& flowStatName : flowStatNames) {
        std::stringstream ss;
        ss << "flow-" << flowId;
        ss << "-" << flowStatName;
        signals[ss.str()] = registerSignal(ss.str().c_str());


         std::stringstream ts;
         ts.clear(); ts.str("");
         ts << "flow" << flowStatName;

         cProperty *statisticTemplate =\
         getProperties()->get("statisticTemplate", ts.str().c_str());
         ev.addResultRecorders(this, signals[ss.str()],\
                 ss.str().c_str(), statisticTemplate);
    }
}

void App::emitFlowSignal(int flowId, const std::string& name, double value) {
    std::stringstream ss;
    ss << "flow-" << flowId;
    ss << "-" << name;
    emitSignal(ss.str(), value);
}

void App::setupSignals() {
    for (const auto& statName : statNames)
        signals[statName] = registerSignal(statName.c_str());
}

void App::emitSignal(std::string name, double value) {
    emit(signals[name], value);
    ev << "at time " << simTime() << " emitting " << name << ": " << value << ".\n";
}

void App::handleMessage(cMessage *msg)
{
    MessageKind timeout = TIMEOUT;
    MessageKind control = CONTROL;
    MessageKind data = DATA;
    MessageKind meta = META;
    SimTime now = simTime();

    if (msg->getKind() == timeout) {
        TimeoutMessage *timeoutMsg = check_and_cast<TimeoutMessage *>(msg);
        EV << "App " << myAddress << ": Got a timeout message for "\
                << timeoutMsg->getFlowId() << ".\n";


        nextPacket(timeoutMsg->getFlowId());

    }
    else if (msg->getKind() == meta) {
        MetaPacket *pkt = check_and_cast<MetaPacket *>(msg);
        int flowId = pkt->getFlowId();
        EV << "App " << myAddress << ": Got a meta message for "\
                << pkt->getFlowId() << ".\n";
        // add an entry for flow from src to dst with xx packets

        FlowInfo f(*pkt);
        assert(f.size >= 1);
        flowTable.insert(std::pair<int, FlowInfo>(flowId, f));



        // send flow size to scheduler
        ControlPacket *requestPkt = makeRequestPacket(*pkt);
        send(requestPkt, "out");

    }  else if (msg->getKind() == control)
    {
        ControlPacket *pkt = check_and_cast<ControlPacket *>(msg);
        ControlPacketType request = REQUEST;
        ControlPacketType grant = GRANT;

        EV << "App " << myAddress << ": Got a control message "\
                << ((pkt->getControlPacketType()==request)? "REQUEST" : "GRANT") << " for flow " << pkt->getFlowId() << ".\n";


        if (pkt->getControlPacketType() == grant)
        {
            int flowId = pkt->getFlowId();

            if (flowTable.count(flowId) == 0) {
                ev << "No such flow.\n";
                delete pkt;
                return;
            } else {
                // update rate for flow's packets
                updateRate(flowId, pkt->getRate());
            }

        }
    }
    else if (msg->getKind() == data) {
        DataPacket *pkt = check_and_cast<DataPacket *>(msg);
        switch (pkt->getDataPacketType()) {
        case JUST_DATA:
        {
            if (flowsWithSignals.count(pkt->getFlowId()) == 0)
                setupFlowSignals(pkt->getFlowId());
            emitFlowSignal(pkt->getFlowId(), "queueSize", pkt->getQueueSize());
            emitFlowSignal(pkt->getFlowId(), "rtt", now.dbl()-pkt->getTs());
            EV << "App " << myAddress << ": Got a JUST_DATA packet for flow "\
                    << pkt->getFlowId() << ".\n";
            // do nothing
            break;
        }

        case FIN:
        {
            EV << "App " << myAddress << ": Got a FIN packet for flow "\
                    << pkt->getFlowId() << ".\n";
            DataPacket* finAck = new DataPacket();
            finAck->setDataPacketType(FINACK);
            finAck->setFlowId(pkt->getFlowId());
            finAck->setSource(pkt->getDestination());
            finAck->setDestination(pkt->getSource());
            send(finAck, "out");
            break;
        }
        case FINACK:
        {
            EV << "App " << myAddress << ": Got a FINACK packet for flow "\
                    << pkt->getFlowId() << ".\n";
            assert(flowTable.count(pkt->getFlowId()) > 0);
            ControlPacket *endNotification = makeRequestPacket(flowTable.at(pkt->getFlowId()).metaPacket);


            endNotification->setSize(0);
            send(endNotification, "out");

            cancelAndDelete(flowTable.at(pkt->getFlowId()).nextPacket);

            flowTable.erase(pkt->getFlowId());

            // Send an endNotification scheduler


            // emitSignal
            break;
        }
        default:
            break;
            // do nothing
        }
    }

    // LOGGING


    if (msg->getKind() == data || msg->getKind() == control || msg->getKind() == meta) {
        Packet *pk = check_and_cast<Packet *>(msg);

        EV << "received packet " << pk->getName() << " after " << pk->getHopCount() << "hops" << endl;

        emit(endToEndDelaySignal, simTime() - pk->getCreationTime());

        emit(hopCountSignal, pk->getHopCount());

        emit(sourceAddressSignal, pk->getSource());


        delete pk;
    } else if (msg->getKind() == timeout){
        EV << "received timeout message.\n";
    }

    if (ev.isGUI())

    {

        getParentModule()->getDisplayString().setTagArg("i",1,"green");

        getParentModule()->bubble("Arrived!");

    }
}



