//
// This file is part of an OMNeT++/OMNEST simulation example.
//
// Copyright (C) 1992-2008 Andras Varga
//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//

#ifdef _MSC_VER
#pragma warning(disable:4786)
#endif

#include <map>
#include <omnetpp.h>
#include "Packet_m.h"
#include "FlowInfo.h"


/**
 * Demonstrates static routing, utilizing the cTopology class.
 */
class FlowGenerator : public cSimpleModule
{
  private:
    int schedulerAddress;
    int packetSize;
    // maybe listeners for when flows send SYN/ FIN etc.

    // maybe need to a optimal rates module
  protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);
    void sendFlowMessages(const std::vector<FlowInfo>& flows);
};

Define_Module(FlowGenerator);


void FlowGenerator::initialize()
{
    schedulerAddress = par("schedulerAddress").doubleValue();
    packetSize = par("packetSize").doubleValue();
    // flowAlert --> addresses


    std::vector<FlowInfo> flows;

    FlowInfo f;
    f.flowId = 0;
    f.source = 1;
    f.destination = 2;
    f.size = 3;
    f.packetSize = packetSize;

    FlowInfo g;
    g.flowId = 1;
    g.source = 1;
    g.destination = 2;
    g.size = 3;
    g.packetSize = packetSize;

    flows.push_back(f);
    flows.push_back(g);
    sendFlowMessages(flows);
}

void FlowGenerator::sendFlowMessages(const std::vector<FlowInfo>& flows) {
    for (const auto& f: flows) {
        std::stringstream ss;
        ss << "start flow (size " << f.size << ", pkt size " << f.packetSize << ") from " << f.source << " to " << f.destination << ".";
        MetaPacket* msg = new MetaPacket(ss.str().c_str());

        msg->setDestination(f.destination);
        msg->setSource(f.source);

        msg->setFlowId(f.flowId);
        msg->setSize(f.size);
        msg->setPacketSize(f.packetSize);

        msg->setMetaPacketType(START);
        msg->setSchedulerAddress(schedulerAddress);

        send(msg, "flowAlert", f.source);
    }
}

void FlowGenerator::handleMessage(cMessage *msg)
{
    // gets no messages
}

