#ifndef FLOW_INFO_H
#define FLOW_INFO_H

#include "Packet_m.h"


class FlowInfo {
public:
    int flowId;
    int source;
    int destination;
    int size;
    int packetsSent;
    double rate;
    SimTime lastPacketSent;

    TimeoutMessage* nextPacket;

    MetaPacket metaPacket;
    int packetSize; // bits

    FlowInfo(const MetaPacket& pkt) : flowId(pkt.getFlowId()), source(pkt.getSource()), destination(pkt.getDestination()),\
            size(pkt.getSize()), packetsSent(0), rate(-1), nextPacket(new TimeoutMessage("timeout")), metaPacket(pkt),\
            packetSize(pkt.getPacketSize())\
            {nextPacket->setFlowId(flowId);}
    FlowInfo() : flowId(-1), source(-1), destination(-1), size(-1), packetsSent(-1), rate(-1), nextPacket(NULL),  packetSize(-1){}
};

#endif
